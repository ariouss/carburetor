��    Q      �  m   ,      �  "   �       (     *   F     q  !   �     �     �     �     �     �     �  
                  %     6     C     K     R     Z  +   b  1   �  ;   �     �     	     	     	  
   	     '	  )   .	     X	     w	      �	      �	  +   �	  !   
     '
     E
     c
     
     �
     �
     �
     �
  ,   �
  
   �
  "   �
       	   6  	   @     J     P     p     x       $   �     �     �     �     �     	          $     3     A  	   I     S     l     u     �     �     �     �     �     �     �     �            �  )     �  	   �     �  !        0     C     W  
   f  	   q     {     �  	   �  
   �     �  	   �     �     �     �     �     �                 6  0   W     �     �     �     �     �     �     �     �  
   �     �     �     �                         %     5     K  	   X  	   b  3   l  	   �     �     �  	   �  	   �  	   �  
   �  	   �     �     �     �          $     6     K     `  	   r     |     �     �  	   �     �  
   �     �     �     �     �  	   �     �            	        &     9     *           '       N      5   K          
       O       #       2       G       0   P   &       >   1      <   6   $           :              !      A   B       +   H   8   L       7   J       )   ;   @         %   =             3   I   C      M             	                 -                   ,   4       D   (      Q                 .                      9      "   /       F       E   ?           Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Chinese (Simplified) (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/carburetor/translations/zh_Hans/>
Language: zh_Hans
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 5.4-dev
 _检查连接 新标识 _切换系统代理 允许外部设备连接此网络 _关于 Carburetor 键盘快捷键(_K) 首选项 (_P) 退出(_Q) 奥地利 自动（最好） 保加利亚 加拿大 Carburetor 正在连接… 捷克语 正在断开连接… 出口国家 芬兰 法国 德国 爱尔兰共和国 Tractor 要监听的本地端口 HTTP 隧道监听的本地端口 您将拥有匿名名称服务器的本地端口 摩尔多瓦 荷兰 无 挪威 混淆 波兰 已允许的连接 网桥 DNS 端口 出口节点 HTTP 端口 可插拔传输 Socks 端口 网桥 一般 端口 代理已设置 代理已取消设置 罗马尼亚 运行中 俄罗斯 将网桥作为文件保存在您的本地配置中 塞舌尔 功能 一般 新加坡 Snowflake 西班牙 取消(_C) 已停止 瑞典 瑞士 您要连接的国家 Tractor 无法连接 Tractor 已连接 Tractor 未运行！ Tractor 正在运行 Tractor 已停止 乌克兰 英国 美国 原生 WebTunnel 你拥有了新身份！ 连接(_C) 断开连接(_D) _OK 保存 连接 / 断开连接 新标识 个人设置 退出 快捷方式 主菜单 显示详细信息 Glitter <glitter@vern.cc> 