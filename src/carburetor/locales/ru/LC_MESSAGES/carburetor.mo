��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *  *        ;  0   L  a   }     �      �          -     9     H     `     q     ~     �  
   �     �     �     �               !  `   2  l   �  d         e     t  5   �     �     �     �  �   �  �   �       )   '  
   Q     \     i     �  -   �     �  
   �  
   �  
   �  !   �          /     >     M  c   Z     �      �  
   �     �  	   
          #     1     F     S  L   f    �  .   �     �     	     &     =  4   Z     �     �     �     �     �     �  &        *     @     T     Z  )   n     �     �     �     �     �     �  (        #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Russian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian <https://hosted.weblate.org/projects/carburetor/translations/ru/>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.4-dev
 _Проверить подключение _Новый ИД _Включить прокси в системе Разрешить внешним устройствам использовать эту сеть _О приложении _Сочетания клавиш _Настройки _Выход Австрия Авто (Лучший) Болгария Канада Карбюратор Подключение… Чехия Отключение… Выходная страна Финляндия Франция Германия Ирландия Локальный порт, на котором будет прослушиваться Tractor Локальный порт, на котором будет прослушиваться HTTP-туннель Локальный порт, на котором будет анонимный сервер имен Молдова Нидерланды Подходящих мостов не найдено Нет Норвегия Обфусцированный Пожалуйста, проверьте мосты на соответствие выбранному типу подключаемого транспорта. Пожалуйста, скопируйте и вставьте имеющиеся у вас мосты в область ниже. Польша Принимать подключения Мосты Порт DNS Выходные узлы Порт HTTP Подключаемые транспорты Порт Socks Мосты Общие Порты Прокси установлен Прокси отключен Румыния Запущен Россия Сохранить мосты в виде файла в локальной конфигурации Сейшелы Функциональность Общие Сингапур Snowflake Испания _Отмена Остановлен Швеция Швейцария Страна, из которой вы хотите подключиться Чтобы получить мосты, у вас есть следующие варианты:

• Посетите сайт Tor: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Используйте Telegram бот Tor bridges : <a href="https://t.me/GetBridgesBot">Telegram бот Tor bridges</a>
• Отправьте письмо с адреса Riseup или Gmail на адрес: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor не смог подключиться Tractor подключен Tractor не запущен! Tractor запущен Tractor остановлен Исполняемый файл транспорта Тип транспорта Украина Великобритания США Обычный Веб-Туннель У вас новая личность! _Подключить _Отключить _ОК _Сохранить Подключить / Отключить Новый ИД Параметры Уйти Ярлыки Главное меню Показать детали Сергей Ворон <voron@duck.com> 