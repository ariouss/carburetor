��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �  7   �  ]   #  "   �  *   �     �  	   �  
   �     �          #     0     C     `     e     �     �     �  
   �     �  6   �  :   �  6   :     q     �  (   �     �     �     �  �   �  N   b     �     �     �     �     �       (   !     J     ^  
   j     u     �      �     �     �  
   �  d   �     W     `  
   o     z     �     �     �     �     �  
   �  =   �  )    +   A     m  -   �  '   �  )   �  /   
     :     Y     h     y     �     �  "   �     �     �  	   �     �     �          $     3     <     P     d  1   |     #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Persian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Persian <https://hosted.weblate.org/projects/carburetor/translations/fa/>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 5.4-dev
 _بررسی اتّصال _هویت جدید _تغییر وضعیت پیشکار روی سامانه اجازه به افزاره‌های خارجی برای استفاده از این شبکه _دربارهٔ کاربراتور _میان‌برهای صفحه‌کلید _ترجیحات _خروج اتریش خودکار (بهترین) بلغارستان کانادا کاربراتور در حال وصل شدن… چک در حال قطع شدن… کشور خروجی فنلاند فرانسه آلمان ایرلند درگاه محلّی برای شنود تراکتور درگاه محلّی برای شنود یک تونل HTTP درگاه محلّی برای ساناد ناشناس مولداوی هلند هیچ پل مربوطی پیدا نشد هیچ‌کدام نروژ مبهم شده لطفاً پل‌ها را برای مطابقت با گونهٔ جابه‌جاگر افزایشی گزیده بررسی کنید. رونوشت و جایگذاری پل‌هایتان در ناحیهٔ زیر. لهستان پذیرش اتّصال‌ها پل‌ها درگاه ساناد گره خروجی درگاه وب جابه‌جاگرهای افزایشی درگاه ساکس پل‌ها عمومی درگاه‌ها پیشکار تنظیم شد پیشکار برداشته شد رومانی در حال اجرا روسیه ذخیرهٔ پل‌ها به شکل یک پرونده در پیکربندی‌های شخصیتان سیشل کاربردی عمومی سنگاپور دانه برفی اسپانیا _انصراف متوقّف سوئد سوییس کشوری که می‌خواهید از آن وصل شوید گزینه‌های زیر برای گرفتن پل‌ها وجود دارند:

• سر زدن به پایگاه وب تور: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• استفاده از روبوت تلگرامی پل‌های تور: <a href="https://t.me/GetBridgesBot">روبوت تلگرامی پل‌های تور</a>
• فرستادن رایانامه از نشانی رایزآپ یا جی‌میل به <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> تراکتور نتوانست وصل شود تراکتور وصل است تراکتور در حال اجرا نیست! تراکتور در حال اجراست تراکتور متوقّف شده است پروندهٔ اجرایی جابه‌جاگر گونهٔ جابه‌جاگر اوکراین بریتانیا آمریکا وانیلی تونل وب یک هویت جدید دارید! _وصل شدن _قطع شدن _قبول _ذخیره قطع یا وصل شدن هویت جدید ترجیحات خروج میان‌برها فهرست اصلی نمایش جزییات دانیال بهزادی <dani.behzi@ubuntu.com> 