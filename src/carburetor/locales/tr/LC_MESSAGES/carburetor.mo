��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �      �  3   	     =     S  
   i  
   t  	        �     �     �  
   �     �     �     �     �  
   �                 1     6   Q  =   �     �     �     �     �     �  
   �  j   
  V   u     �     �  
   �     �          %  "   >     a  
   {     �     �     �      �     �     �     �  ;   �  
   2     =     K     Q  	   Z     d     m  
   u     �  	   �  (   �  �  �     �     �     �     �     �     �                    1     N  	   S     ]     w     �     �     �     �     �  	   �     �     �  	   �     �          #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Turkish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish <https://hosted.weblate.org/projects/carburetor/translations/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4-dev
 Bağlantıyı _denetle _Yeni kimlik Sistemde vekil sunucu _aç/kapat Harici aygıtların bu ağı kullanmasına izin ver Carburetor _Hakkında _Klavye Kısayolları _Tercihler _Çıkış Avusturya Otomatik (En iyisi) Bulgaristan Kanada Carburetor Bağlanıyor… Çekya Bağlantı kesiliyor… Çıkış Ülkesi Finlandiya Fransa Almanya İrlanda Tractor'un dinleyeceği yerel bağlantı noktası HTTP tünelinin dinleyeceği yerel bağlantı noktası Anonim bir ad sunucunuzun olacağı yerel bağlantı noktası Moldova Hollanda İlgili köprü bulunamadı Yok Norveç Gizlenmiş Lütfen seçilen değiştirilebilir taşıyıcı türüyle eşleşmesi için köprüleri gözden geçirin. Lütfen sahip olduğunuz köprüleri kopyalayın ve aşağıdaki alana yapıştırın. Polonya Bağlantıları Kabul Et Köprüler DNS Bağlantı Noktası Çıkış Düğümü HTTP Bağlantı Noktası Değiştirilebilir taşıyıcılar Socks Bağlantı Noktası Köprüler Genel Bağlantı Noktaları Ağ geçidi ayarlandı Ağ geçidi ayarı kaldırıldı Romanya Çalışıyor Rusya Köprüleri yerel bir yapılandırma dosyası olarak kaydet Seyşeller İşlevsellik Genel Singapur Snowflake İspanya _İptal Durduruldu İsveç İsviçre Üzerinden bağlanmak istediğiniz ülke Köprü edinmek için kullanabileceğiniz seçenekler:

• Tor web sitesini ziyaret edin: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Tor köprüleri Telegram botunu kullanın: <a href="https://t.me/GetBridgesBot">Tor köprüleri Telegram botu</a>
• Bir Riseup veya Gmail adresinden şuraya bir e-posta gönderin: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor bağlanamadı Tractor bağlandı Tractor çalışmıyor! Tractor çalışıyor Tractor durduruldu Taşıyıcı programı Taşıyıcı türü Ukrayna Birleşik Krallık Amerika Birleşik Devletleri Sade WebTunnel Yeni bir kimliğiniz var! _Bağlan Bağlantıyı _kes _Tamam _Kaydet Bağlan / Bağlantıyı Kes Yeni Kimlik Tercihler Çık Kısayollar Ana Menü Ayrıntıları göster Oğuz Ersen <oguz@ersen.moe> 