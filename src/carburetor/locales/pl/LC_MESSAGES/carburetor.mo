��    N      �  k   �      �  "   �     �  (   �  *        9  !   Y     {     �     �     �     �     �  
   �     �     �     �     �                    "  +   *  1   V  ;   �     �     �     �     �  
   �     �  )   �      	     ?	      _	      �	  +   �	  !   �	     �	     
     +
     G
     O
     W
  ,   ^
  
   �
  "   �
     �
  	   �
  	   �
     �
     �
                 $   +     P     i     ~     �     �     �     �     �     �  	   �     �               !     %     +     ;     P     ^     q     �     �  �  �          �     �  <   �               &  	   2     <     D  	   ^     h  
   o     z     �     �     �  	   �     �     �     �  6   �  5     8   =  	   v     �     �     �     �     �     �     �     �     �  	   �     �                         $     ,     9  :   ?     z     �     �     �  	   �  	   �     �  
   �     �  
   �  (   �  $        (     A     V     f     �     �     �     �  	   �     �  	   �     �     �     �                     )     1     ?  K   R     5          9                         G      J   *       &           7      <                     %   )          $   F      6   M   
      #          ?           "   @   +   D   B   ,   '      =              K      3      2   4       L   :              E       ;      !   (   1         C                        8       A   >                N              	   I   .      /       0       -      H    Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Polish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Polish <https://hosted.weblate.org/projects/carburetor/translations/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.4-dev
 _Sprawdź połączenie _Nowy identyfikator _Przełącz proxy w systemie Zezwól urządzeniom zewnętrznym na korzystanie z tej sieci _O Carburetor _Skróty klawiszowe _Ustawienia Za_kończ Austria Automatycznie (najlepiej) Bułgaria Kanada Carburetor Łączenie… Czechy Rozłączanie… Kraj wyjścia Finlandia Francja Niemcy Irlandia Port lokalny, na którym Tractor będzie nasłuchiwał Port lokalny, na którym nasłuchiwał by tunnel HTTP Port lokalny, na którym miałbyś anonimowy serwer nazw Mołdawia Holandia Brak Norwegia Zaciemnione Polska Zaakceptuj połączenia Mostki Port DNS Węzły wyjściowe Port HTTP Transporty wtykowe Port skarpet Mostki Ogólne Porty Rumunia Działające Rosja Zapisz Bridges jako plik w swoich lokalnych konfiguracjach Seszele Funkcjonalny Ogólne Singapur Snowflake Hiszpania _Anuluj Zatrzymane Szwecja Szwajcaria Kraj, z którego chcesz się połączyć Tractor nie potrafi połączyć się Tractor jest połączony Tractor nie biegnie! Tractor biegnie Tractor zostaje zatrzymany Ukraina Wielka Brytania Stany Zjednoczone Wanilia WebTunnel Masz nową tożsamość! Połą_cz _Zerwij połączenie _OK _Zapisz Nowy identyfikator Preferencje Zakończ Skróty Menu główne Pokaż szczegóły Radek Raczkowski, Adam Hulewicz, Radosław Korotkiewicz, David Troianovskyi 