��    U      �  q   l      0  "   1     T  (   m  *   �     �  !   �               2     :     F     O  
   V     a     o     u     �     �     �     �     �  +   �  1   �  ;   	     L	     T	     `	     z	     	  
   �	  =   �	     �	  )   �	      
     
      ?
      `
  +   �
  !   �
     �
     �
          '     :     O     W     _  ,   f  
   �  "   �     �  	   �  	   �     �     �                '  $   3  �  X     �               0     C     V     h     p          �  	   �     �     �     �     �     �     �     �               (     ;     M     b  �  u       	   '  #   1  9   U     �     �     �     �     �     �     �     �  
             $     )     D  	   S     ]     e     m  ,   u  5   �  6   �               $     C     K  	   T  I   ^     �     �     �  	   �     �  
   �     �  
                        &     B     b     j     x  6     
   �     �     �  	   �  	   �     �     �     �                  �  5  %   �     �     
           7     M     _     g     s       	   �     �  	   �     �     �     �     �     �  
   �     �               "     2     D   ?      C           ;   L   I       A   (   H      <   B   E   O                   "   +   J   #                 9       0             F   G   %                     .               *      5      -       S   =   N       3      1   @   )       $   P   8      2   :   	                          '       Q   !       4       
   >                    7          U   6                 /   ,   K   R   M      T       &        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Italian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Italian <https://hosted.weblate.org/projects/carburetor/translations/it/>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4
 _Controllare la connessione _Nuovo ID _Attiva/disattiva proxy sul sistema Consenti ai dispositivi esterni di utilizzare questa rete _Informazioni su Carburator _Scorciatoie tastiera _Preferenze _Esci Austria Automatico (migliore) Bulgaria Canada Carburetor Connessione in corso… Ceco Disconnessione in corso… Esci dal Paese Finlandia Francia Tedesco Irlanda Porta locale su cui Tractor sarà in ascolto Porta locale su cui sarebbe in ascolto un tunnel HTTP Porta locale su cui avresti un server dal nome anonimo Moldavia Paesi Bassi Nessun ponte rilevante Trovato Nessuno Norvegia Offuscato Si prega di copiare e incollare i ponti che avete nella zona sottostante. Polonia Accetta connessioni Bridge Porta DNS Nodi di uscita Porta HTTP Trasporti collegabili Porta Sock Bridge Generale Porte Il proxy è stato impostato Il proxy non è stato impostato Romania In esecuzione Russia Salva Bridge come file nelle tue configurazioni locali Seychelles Funzionalità Generale Singapore Snowflake Spagna A_nnulla Fermato Svezia Svizzera Il Paese da cui vuoi connetterti Per ottenere ponti, avete queste opzioni:

• Visita il sito web Tor: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Utilizzare i ponti Tor Telegram bot: <a href="https://t.me/GetBridgesBot">Colpo di Torre Telegram bot</a>
• Invia un'email da un indirizzo Riseup o Gmail a: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor non è riuscito a connettersi Tractor è connesso Tractor non si avvia! Tractor è in funzione Tractor si è fermato Tipo di trasporto Ucraina Regno Unito Stati Uniti Vanilla WebTunnel Hai una nuova identità! _Connetti _Disconnetti _OK _Salva Connetti / Disconnetti Nuovo ID Preferenze Non aiutarli. Scorciatoie Menù principale Mostra dettagli albanobattistella 