��    R      �  m   <      �  "   �       (   -  *   V     �  !   �     �     �     �     �            
        !     /     5     F     S     [     b     j  +   r  1   �  ;   �     	     	      	     %	  
   ,	     7	  )   >	     h	     �	      �	      �	  +   �	  !   
     7
     U
     s
     �
     �
     �
     �
     �
  ,   �
  
   �
  "        )  	   F  	   P     Z     `     �     �     �  $   �     �     �     �               ,     >     F     U     c  	   k     u     �     �     �     �     �     �     �     �     �          #     8  �  K     �  
   �  "     A   $     f          �     �     �     �     �     �  
   �     �  	   �     �     	          !  	   (     2  -   :  4   h  >   �     �     �     �     �  	   �               '     -     6  	   F     P  
   g     r  	   x     �     �     �     �     �     �  C   �  
        %  	   5  	   ?  	   I     S     [     d     m     t  4   {  !   �     �     �               .     @     H     T     `  	   h  #   r  
   �     �     �     �     �  	   �     �     �  
   �               )     *           '       O      5   L          
       P       #       2       H       0   Q   &       >   1      <   6   $           :              !      B   C       +   I   8   M       7   K       )   ;   A         %   =             3   J   D      N   @         	                 -                   ,   4       E   (      R                 .                      9      "   /       G       F   ?           Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: French (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: French <https://hosted.weblate.org/projects/carburetor/translations/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 5.4
 _Vérifier la connexion _Nouvel ID _Basculer le proxy sur le système Autoriser ou non les appareils extérieurs à utiliser ce réseau _À propos de Carburetor _Raccourcis clavier _Préférences _Quitter Autriche Auto (recommandé) Bulgarie Canada Carburetor Connexion… Tchéquie Déconnexion… Pays de sortie Finlande France Allemagne Irlande Port local via lequel Tractor serait écouté Port local sur lequel un tunnel HTTP serait écouté Port local pour un serveur DNS anonyme que vous pourriez avoir Moldavie Pays-Bas Aucun Norvège Obfusqué Pologne Accepter des connexions Ponts Port DNS Nœud de sortie Port HTTP Transports enfichables Port SOCKS Ponts Général Ports Proxy a été réglé Proxy a été désactivé Roumanie En exécution Russie Enregistrer les ponts dans un fichier de votre configuration locale Seychelles Fonctionnalité Général Singapour Snowflake Espagne _Annuler Arrêté Suède Suisse Le pays depuis lequel vous voulez être connecté·e Tractor n’a pas pu se connecter Tractor est connecté Tractor est à l'arrêt ! Tractor est en marche Tractor est arrêté Type de transport Ukraine Royaume-Uni États-Unis Vanille WebTunnel Vous avez une nouvelle identité ! _Connecter _Déconnecter _OK _Enregistrer Connexion / Déconnexion Nouvel ID Préférences Quitter Raccourcis Menu principal Afficher les détails – 