��    T      �  q   \         "   !     D  (   ]  *   �     �  !   �     �          "     *     6     ?  
   F     Q     _     e     v     �     �     �     �  +   �  1   �  ;    	     <	     D	     P	     j	     o	  
   v	  H   �	     �	  )   �	     �	     
      :
      [
  +   |
  !   �
     �
     �
          "     5     =     E  ,   L  
   y  "   �     �  	   �  	   �     �     �     �            $     �  >     �     �     �          )     <     N     V     e     s  	   {     �     �     �     �     �     �     �     �                !     3     H  �  [     �          "  9   ?     y     �     �     �     �     �  	   �     �  
   �     �     �          !  
   1     <     D     M  /   U  2   �  ,   �  	   �     �  "   �     !     (     0  \   9     �     �     �     �     �     �     �     �                         +     4     D  ?   L  	   �     �     �  	   �  	   �     �  	   �     �     �     �     �  �    $   �     �  !   �     �          /     B     K     W     r  	   z     �  	   �     �     �     �     �     �     �     �     �     �       ^        C   >      B           :   K   H       @   (   G      ;   A   D   N                   "   +       #                 8       /             E   F   %                     -               *      4      M       R   <           2      0   ?   )   I   $   O   7      P   9   	                          '       T   !       3       
   =   1                6              5                 .   ,   J   Q   L      S       &        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Portuguese (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <https://hosted.weblate.org/projects/carburetor/translations/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 5.4
 _Verifique a conexão _Nova identidade (ID) _Alternar o proxy no sistema Permitir ou não que os aparelhos externos usem esta rede Sobre o _Carburetor Atalhos do _teclado _Preferências _Sair Áustria Automático (melhor) Bulgária Canadá Carburetor A conectar… República Checa A desconectar… País da saída Finlândia França Alemanha Irlanda Porta local na qual o Tractor estará à escuta Porta local a qual o túnel HTTP estará à escuta Porta local do servidor de nome anónimo DNS Moldávia Países Baixos Nenhuma ponte relevante encontrada Nenhum Noruega Ofuscado Por favor, verifique as pontes para combinar com o tipo de transporte plugável selecionado. Polónia Aceitar conexão Pontes Porta de DNS Nó de saída Porta de HTTP Transporte plugável Porta socks Pontes Geral Portas Um proxy foi definido Roménia A ser executado Rússia Guardar pontes como um ficheiro nas suas configurações locais Seicheles Funcionalidade Geral Singapura Snowflake Espanha _Cancelar Parado Suécia Suíça O país de onde quer ligar Para obter pontes, você tem essas opções:

• Visite o website do Tor: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use as pontes Tor Telegram bot: <a href="https://t.me/GetBridgesBot">Tor pontes Telegram bot</a>
• Enviar um e-mail de um endereço Riseup ou Gmail para: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> O Tractor não conseguiu conectar-se Tractor está conectado Tractor não está em execução! Tractor está em execução Tractor está parado Tipo de transporte Ucrânia Reino Unido Estados Unidos da América Vanilla WebTunnel Tem uma nova identidade! _Conectar _Desconectar _Aceitar _Guardar Conectar/Desconectar Nova ID Preferências Sair Atalhos Menu Principal Mostrar detalhes Manuela Silva <mmsrs@sky.com>
Sérgio Morais <lalocas@protonmail.com>
Ssantos <ssantos@web.de> 