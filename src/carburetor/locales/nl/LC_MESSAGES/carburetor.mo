��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �     �  3        ;     L     Y  
   e  
   p     {  	   �     �  
   �     �  
   �     �     �     �  	     	          ,     ;   J  ;   �  
   �  	   �  #   �     �  	         
  >     1   Q     �     �     �  	   �     �  
   �     �  
   �     �     �               &  	   ?     I     P  6   X  
   �     �     �  	   �  
   �     �  
   �     �     �     �  ,   �  �  "  !   �     �     �          %     8     L  	   \     f     z  	   �  	   �     �  
   �     �     �     �     �          %  	   ;     E  	   Q     [     h     #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Dutch (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Dutch <https://hosted.weblate.org/projects/carburetor/translations/nl/>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4-dev
 Verbinding _controleren _Nieuw identiteitsbewijs Systeemproxy aan/ui_t Externe apparaten toestaan dit netwerk te gebruiken _Over Carburetor _Sneltoetsen _Voorkeuren _Afsluiten Oostenrijk Automatisch (beste) Bulgarije Canada Carburetor Bezig met verbinden… Tsjechiẽ Verbinding aan het verbreken… Afsluitland Finland Frankrijk Duitsland Ierland De lokale poort waarop Tractor zal luisteren De lokale poort waarop een http-tunnel zou kunnen luisteren De lokale poort waarop u een anonieme naamserver kan hebben Moldaviẽ Nederland Geen relevante bruggen aangetroffen Geen Noorwegen Anoniem Controleer of de bruggen overeenkomen met het overdrachtstype. Kopieer en plak alle bruggen in onderstaand veld. Polen Verbindingen toestaan Bruggen Dns-poort Afsluitpunten Http-poort Pluggable transports Sockspoort Bruggen Algemene instellingen Poorten De proxy is ingesteld De proxy is losgekoppeld Roemenië Actief Rusland Sla bridges op als een bestand in uw lokale voorkeuren Seychellen Functionaliteit Algemeen Singapore Sneeuwvlok Spanje _Annuleren Gestopt Zweden Zwitserland Het land van waaruit u verbinding wilt maken Bruggen kunnen op een van de volgende manieren worden verkregen:

• Via de Tor-website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Via de Tor Telegram-bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Via een e-mail van een Riseup- of Gmail-adres naar: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor kan geen verbinding maken Tractor is verbonden Tractor is niet actief! Tractor is actief Tractor is gestopt Uitvoerbaar bestand Overdrachtstype Oekraïne Verenigd Koninkrijk Verenigde Staten Standaard WebTunnel U heeft een nieuwe identiteit! _Verbinden Ver_binding verbreken _Oké Op_slaan Verbinden/Verbinding verbreken Nieuw identiteitsbewijs Voorkeursinstellingen Afsluiten Sneltoetsen Hoofdmenu Toon Details Philip Goto, Heimen Stoffels 