��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �     �  3   �     ,     ;     R     b  
   k     v  	   �     �  
   �     �     �     �     �     �  	   �     �     �  %   �  6     7   R     �     �     �     �     �     �  G   �  B        b     h     ~     �     �  	   �     �  
   �     �     �     �     �     �  	               6        S     `     l  	   u  	        �     �     �     �     �      �  �  �     z     �     �     �     �     �     �     �               (  	   8     B     Z     b     q     w     ~     �     �     �  	   �  	   �     �  J   �     #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Swedish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Swedish <https://hosted.weblate.org/projects/carburetor/translations/sv/>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4-dev
 _Kolla anslutning _Ny ID _Växla proxy på systemet Tillåt externa enheter att använda detta nätverk _Om Carburetor _Tangentbordsgenvägar _Inställningar _Avsluta Österrike Auto (bästa) Bulgarien Kanada Carburetor Ansluter… Tjeckien Kopplar från… Utgångsland Finland Frankrike Tyskland Irland Lokal port som Tractor ska lyssna på Lokal port på vilken en HTTP-tunnel skulle lyssna på Lokal port på vilken du skulle ha en anonym namnserver Moldova Nederländerna Inga relevanta broar hittades Ingen Norge Fördunklad Kontrollera broarna för att matcha den valda pluggbara transporttypen. Vänligen kopiera och klistra in de broar du har i området nedan. Polen Godkänn anslutningar Broar DNS Port Utgångsnode HTTP Port Pluggbara transporter Socks Port Broar Allmänt Portar Proxy har ställts Proxy har ställts av Rumänien Kör Ryssland Spara bryggor som en fil i dina lokala konfigurationer Seychellerna Funktionell Allmänt Singapore Snowflake Spanien _Avbryt Stoppad Sverige Schweiz Landet som du vill ansluta från För att få broar har du dessa alternativ:

• Besök Tors webbplats: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Använd Tor broars Telegram bot: <a href="https://t.me/GetBridgesBot">Tor broars Telegram bot</a>
• Skicka ett e-postmeddelande från en Riseup eller Gmail-adress till: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor kunde inte koppla Tractor är kopplad Tractor kör inte! Tractor kör Tractor är stoppad Körbar transport Typ av transport Ukraina Storbritannien Förenta staterna Utan ändringar WebTunnel Du har en ny identitet! _Anslut _Koppla ifrån _Okej _Spara Anslut / Avkoppla Ny ID Inställningar Avsluta Genvägar Huvudmeny Visa detaljerat skøldis <carburetor@turtle.garden>
Luna Jernberg <lunajernberg@gnome.org> 