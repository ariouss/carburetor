��    V      �     |      x  "   y     �  (   �  *   �     	  !   )     K     f     z     �     �     �  
   �     �     �     �     �     �     �     �     �  +   �  1   &	  ;   X	     �	     �	     �	     �	     �	  
   �	  H   �	  =   "
     `
  )   g
     �
     �
      �
      �
  +     !   >     `     ~     �     �     �     �     �     �  ,   �  
   $  "   /     R  	   o  	   y     �     �     �     �     �  $   �  �  �     {     �     �     �     �     �     �                 	   &     0     I     R     ^     b     h     �     �     �     �     �     �     �  �       �     �     �  @   �          '     8     N     W     c  	   y     �  
   �     �  
   �     �     �     �  
   �     �       +     :   :  3   u     �     �  "   �     �     �  
   �  ]   �  S   V     �     �     �     �     �  	   �      �  
             &     2  "   8  $   [  	   �     �     �  @   �  
   �     �                     "     *  
   6     A     J  9   R  �  �  *   K     v     �     �     �     �     �     �          !  	   )  ,   3  
   `     k     t  
   x     �     �     �     �     �  
   �     �     �     #   :       /   F   =                9   2   I   A          G          
           '   ;      *   0   L          &      !           R   "   6      M       E             B       @   $   1                 N   %   K   8              D   P       V                      Q              >   4           .              (                          U       <      3       )                +   ,          S       O          7   ?   H   5             	   C                 J   -   T        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: German (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: German <https://hosted.weblate.org/projects/carburetor/translations/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4
 _Verbindung prüfen _Neue ID Proxy auf System umschal_ten Ermöglichen Sie externen Geräten, dieses Netzwerk zu verwenden _Über Carburetor _Tastaturkürzel Ada_ptereinstellungen _Beenden Österreich Automatisch (Optimal) Bulgarien Kanada Carburetor Verbindung wird erstellt … Tschechien Verbindung wird getrennt … Ausgang Land Finnland Frankreich Deutschland Irland Lokaler Port, auf dem Tractor lauschen soll Lokaler Port, an dem ein HTTP-Tunnel abgehört werden soll Lokaler Port, auf dem der anonyme DNS-Server läuft Moldau Niederlande Keine relevanten Brücken gefunden Kein Norwegen Obfuskiert Bitte überprüfen Sie die Brücken, um den ausgewählten steckbaren Transporttyp anzupassen. Bitte kopieren und einfügen Sie die Brücken, die Sie in der Umgebung unten haben. Polen Verbindungen Akzeptieren Brücken DNS-Port Exit-Knoten HTTP-Port Austauschbare Übertragungsarten Socks-Port Brücken Allgemeines Ports Stellvertreter hat gewesen gesetzt Stellvertreter hat gewesen ungefasst Rumänien Wird ausgeführt Russland Speichern Sie Bridges als Datei in Ihren lokalen Konfigurationen Seychellen Funktionalität Allgemeines Singapur Schneeflocke Spanien _Schließen Angehalten Schweden Schweiz Das Land, aus dem Sie eine Verbindung herstellen möchten Um Brücken zu erhalten, haben Sie diese Optionen:

• Besuchen Sie die Tor-Website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Verwenden Sie die Tor Brücken Telegram Bot: <a href="https://t.me/GetBridgesBot">Tor Brücken Telegram Bot</a>
• Senden Sie eine E-Mail von einer Riseup- oder Gmail-Adresse an: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Traktor konnte keine Verbindung herstellen Tractor ist verbunden Tractor ist ausgeschaltet! Tractor wird ausgeführt Tractor wurde angehalten Art der Beförderung Ukraine Vereinigtes Königreich Vereinigte Staaten Vanille WebTunnel Ihnen wurde eine neue Identität zugeordnet! _Verbinden _Trennen _OK _Speichern Verbinden / Trennen Neue ID Einstellungen Hilf ihnen nicht. Tastenkürzel Hauptmenü Details anzeigen Übersetzer Anerkennungen 