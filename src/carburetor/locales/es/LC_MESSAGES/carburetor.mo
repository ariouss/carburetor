��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �      �  H        T     j     }     �     �     �     �     �  
   �     �     �     �     �  	         
            2   #  7   V  @   �     �     �  '   �                 a   '  D   �     �     �     �     �     �               4     D     L     T     \     w     �     �     �  =   �  
   �     �            	           	   (     2     9     @  (   F  �  o     -     H     `     �     �     �     �     �     �     �     �  	          
   )     4     A     J     S     j     �     �     �     �     �     �     #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Spanish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Spanish <https://hosted.weblate.org/projects/carburetor/translations/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4-dev
 _Verificar conexión _Identidad nueva _Alternar el proxy en el sistema Indica si debe permitirse a los dispositivos externos el uso de esta red _Acerca de Carburetor _Atajos de teclado _Preferencias _Salir Austria Automático (el mejor) Bulgaria Canadá Carburetor Conectando… Chequia Desconectando… Salir País Finlandia Francia Alemania Irlanda Puerto local en el que Tractor estaría escuchando El puerto local en el que se escucharía un túnel HTTP Puerto local en el cual tendrías un servidor de nombre anónimo Moldavia Países Bajos No se han encontrado puentes relevantes Ninguno Noruega Ofuscado Por favor, compruebe que los puentes coincidan con el tipo de transporte enchufable seleccionado. Por favor, copie y pegue los puentes que tenga en el área de abajo. Polonia Aceptar conexión Puentes Puerto de DNS Nodo de salida Puerto de HTTP Transportes conectables Puerto de SOCKS Puentes General Puertos Se ha configurado el proxy Se ha desactivado el proxy Rumania Ejecutándose Rusia Guarde Bridges como un archivo en sus configuraciones locales Seychelles Funcionalidad General Singapur Snowflake España _Cancelar Parado Suecia Suiza El país desde el cual quiere conectarse Para conseguir puentes, tienes estas opciones:

• Visite el sitio web de Tor: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Utiliza el bot de Telegram de Tor bridges: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Envíe un correo electrónico desde una dirección de Riseup o Gmail a: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor no pudo conectarse Tractor está conectado No se está ejecutando Tractor! Tractor está ejecutándose Tractor se detuvo Archivo ejecutable Medio de transporte Ucrania Reino Unido Estados Unidos Vanilla Tor WebTunnel Tiene una identidad nueva! _Connectar _Desconectar _Aceptar _Guardar Conectar / Desconectar Nueva identificación Preferencias No los ayudes. Atajos Menú principal Mostrar los detalles reconocimiento-traductor 