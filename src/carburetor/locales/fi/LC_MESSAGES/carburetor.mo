��    Q      �  m   ,      �  "   �       (     *   F     q  !   �     �     �     �     �     �     �  
                  %     6     C     K     R     Z  +   b  1   �  ;   �     �     	     	     	  =   	     Z	  )   a	     �	     �	      �	      �	  +   
  !   8
     Z
     x
     �
     �
     �
     �
     �
  ,   �
  
   	  "        7  	   T  	   ^     h     n     �     �     �  $   �     �     �     �          '     :     L     T     c     q  	   y     �     �     �     �     �     �     �     �     �               1     F  �  Y     �        3   	  6   =     t     �  
   �     �  	   �     �     �     �  
   �     �     �               %     +     2     8  +   @  0   l  @   �     �  
   �     �     �  /        3     9     R  
   Y     d     s          �     �     �     �     �     �     �     �  3   �  
   0     ;     K  	   S  	   ]     g     o     x     �     �     �     �     �     �     �          -     @     H     a     m  	   u       	   �     �     �  	   �     �     �  	   �     �     �     
          &     *           '       N      4              
       O       #       1       G       /   P   &       =   0      ;   5   $           9              !      A   B       K   H   7   L       6   J       )   :   @         %   <             2   I   C      M   ?         	                 ,                   +   3       D   (      Q                -                       8      "   .       F       E   >           Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Finnish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Finnish <https://hosted.weblate.org/projects/carburetor/translations/fi/>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4
 _Tarkista yhteys _Uusi ID _Vaihda järjestelmän välityspalvelimen välillä Salli ulkoisten laitteiden yhdistää tähän verkkoon _Tietoja - Carburetor _Pikanäppäimet _Asetukset _Lopeta Itävalta Automaattinen (paras) Bulgaria Kanada Carburetor Yhdistetään… Tšekki Katkaistaan yhteys… Poistumismaa Suomi Ranska Saksa Irlanti Paikallinen portti, jossa Tractor kuuntelee Paikallinen portti, jossa HTTP-tunneli kuuntelee Paikallinen portti, jossa haluat anonyymin nimipalvelimen olevan Moldova Alankomaat Ei mitään Norja Kopioi ja liitä sillat alla olevaan alueeseen. Puola Yhteyksien hyväksyminen Sillat DNS-portti Poistumissolmu HTTP-portti Pluggable Transport -tekniikat Socks-portti Sillat Yleistä Portit Välityspalvelin on asetettu Romania Käynnissä Venäjä Tallenna sillat tiedostoksi paikallisiin asetuksiin Seychellit Toiminnallisuus Yleiset Singapore Snowflake Espanja _Peruuta Pysäytetty Ruotsi Sveitsi Maa, josta haluat yhdistää Tractor ei voinut yhdistää Tractor on yhdistetty Tractor ei ole käynnissä! Tractor on käynnissä Tractor on pysäytetty Kuljetuksen tyyppi Ukraina Yhdistynyt kuningaskunta Yhdysvallat Vanilja WebTunnel Sinulla on uusi identiteetti! _Yhdistä _Katkaise yhteys _OK _Tallenna Yhdistä / katkaise yhteys Uusi ID Asetukset Lopeta Pikanäppäimet Päävalikko Näytä tiedot Jiri Grönroos, 2023 