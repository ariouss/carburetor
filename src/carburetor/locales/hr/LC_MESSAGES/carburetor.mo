��    Q      �  m   ,      �  "   �       (     *   F     q  !   �     �     �     �     �     �     �  
                  %     6     C     K     R     Z  +   b  1   �  ;   �     �     	     	     	  
   	     '	  )   .	     X	     w	      �	      �	  +   �	  !   
     '
     E
     c
     
     �
     �
     �
     �
  ,   �
  
   �
  "   �
       	   6  	   @     J     P     p     x       $   �     �     �     �     �     	          $     3     A  	   I     S     l     u     �     �     �     �     �     �     �     �            �  )                )  0   H     y     �  	   �     �     �     �     �     �  
   �     �     �               !  	   (  	   2     <  4   B  7   w  D   �  	   �  
   �     	  	     	        $     ,     :     B     U     b     v     �     �     �     �     �     �  	   �  	   �     �  6   �     6     ?     N     T  	   ]     g  	   s  	   }     �  
   �  "   �     �     �     �               /     8     O     b  	   j     t     �  
   �     �     �     �     �     �     �     �     �     �     �     *           '       N      5   K          
       O       #       2       G       0   P   &       >   1      <   6   $           :              !      A   B       +   H   8   L       7   J       )   ;   @         %   =             3   I   C      M             	                 -                   ,   4       D   (      Q                 .                      9      "   /       F       E   ?           Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Croatian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Croatian <https://hosted.weblate.org/projects/carburetor/translations/hr/>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.4-dev
 _Provjeri vezu _Novi ID _Uklj./Isklj. proxy na sustavu Dopusti vanjskim uređajima koristiti ovu mrežu _Carburetor podaci _Tipkovni prečaci _Postavke _Zatvori Austrija Automatski (najbolje) Bugarska Kanada Carburetor Povezivanje … Češka Odspajanje … Zemlja izlaza Finska Francuska Njemačka Irska Lokalni priključak na kojem se Tractor prisluškuje Lokalni priključak na kojem se HTTP tunel prisluškuje Lokalni priključak na kojem se može nalaziti anonimni poslužitelj Moldavija Nizozemska Ništa Norveška Pomućeno Poljska Prihvati veze Mostovi Priključak za DNS Čvor izlaza Priključak za HTTP Priključni transporti Priključak za Socks Mostovi Opće Priključci Proxy je aktiviran Proxy je deaktiviran Rumunjska Pokrenuto Rusija Spremi mostove kao datoteku u lokalnim konfiguracijama Sejšeli Funkcionalnost Opće Singapur Snowflake Španjolska _Odustani Prekinuto Švedska Švicarska Zemlja iz koje se želiš povezati Tractor se nije uspio povezati Tractor je povezan Tractor nije pokrenut! Tractor je pokrenut Tractor je prekinut Ukrajina Ujedinjeno Kraljevstvo Sjedinjene Države Vanilla WebTunnel Imaš novi identitet! _Poveži _Odspojeno _U redu _Spremi Spoji/Odspoji Novi ID Postavke Zatvori Prečaci Glavni izbornik Pokaži detalje Milo Ivir <mail@milotype.de> 