��    W      �     �      �  "   �     �  (   �  *   �       !   9     [     v     �     �     �     �  
   �     �     �     �     �     �     �     �     	  +   
	  1   6	  ;   h	     �	     �	     �	     �	     �	  
   �	  H   �	  =   2
     p
  )   w
     �
     �
      �
        +   "  !   N     p     �     �     �     �     �     �        ,     
   4  "   ?     b  	     	   �     �     �     �     �     �  $   �  �  �     �     �     �     �     �     �               %     4     B  	   J     T     m     v     �     �     �     �     �     �     �     �            �  *     �     �     �  A        Y     h     {     �  
   �     �     �     �  
   �     �     �     �     �     �  	               !     0   =  +   n     �  	   �     �     �     �  	   �  :   �  1        J     P     a     g     p  	   |     �  
   �     �     �     �     �     �     �     �       (        6     B     Q  	   Z  	   d     n     u     }     �     �  "   �  �  �     W     t     �     �     �     �     �     �     �     �       	   #     -  
   E  
   P     [     _     f     v  
   |     �  	   �  	   �     �  =   �     #   :       /   G   =                9   2   J   A          H          
           '   ;      *   0   M          &      !           S   "   6      N       F             B       @   $   1                 O   %   L   8      D       E   Q       W                      R              >   4           .              (                          V       <      3       )                +   ,          T       P          7   ?   I   5             	   C                 K   -   U        Action Menu Item_Check connection Action Menu Item_New ID Action Menu Item_Toggle proxy on system Allow external devices to use this network App Menu Item_About Carburetor App Menu Item_Keyboard Shortcuts App Menu Item_Preferences App Menu Item_Quit Austria Auto (Best) Bulgaria Canada Carburetor Connecting… Czech Disconnecting… Exit Country Finland France Germany Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands No relevant bridges Found None Norway Obfuscated Please check the bridges to match the selected pluggable transport type. Please copy and paste the bridges you have in the area below. Poland PreferencesGroup titleAccept Connections PreferencesGroup titleBridges PreferencesGroup titleDNS Port PreferencesGroup titleExit Node PreferencesGroup titleHTTP Port PreferencesGroup titlePluggable transports PreferencesGroup titleSocks Port PreferencesPage titleBridges PreferencesPage titleGeneral PreferencesPage titlePorts Proxy has been set Proxy has been unset Romania Running Russia Save Bridges as a file in your local configs Seychelles ShortcutsGroup titleFunctionality ShortcutsGroup titleGeneral Singapore Snowflake Spain StatusPage button label_Cancel Stopped Sweden Switzerland The country you want to connect from To Get bridges, you have these options:

• Visit the Tor website: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Use the Tor bridges Telegram bot: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send an email from a Riseup or Gmail address to: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Transport excutable Type of transport Ukraine United Kingdom United States Vanilla WebTunnel You have a new identity! _Connect _Disconnect _OK _Save shortcutConnect / Disconnect shortcutNew ID shortcutPreferences shortcutQuit shortcutShortcuts tooltipMain Menu tooltipShow Details translator-credits Project-Id-Version: Norwegian Bokmål (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/carburetor/translations/nb_NO/>
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.4-dev
 _Sjekk tilkobling _Ny ID _Slå mellomtjener på systemet Hvorvidt eksterne enheter skal tillates å bruke dette nettverket _Om Carburetor _Tastatursnarveier _Innstillinger _Avslutt Østerrike Auto (best) Bulgaria Canada Carburetor Kobler til … Tsjekkia Kobler fra … Avslutt land Finland Frankrike Tyskland Irland Lokal port Tractor skal lytte til Lokal port som en HTTP-tunnel vil bli lyttet til Lokal port du har en anonym navnetjener på Moldova Nederland Fant ingen relevante bruer Ingen Norge Tilslørt Sjekk at bruene passer med valgt utskiftbar transporttype. Kopier og lim inn bruene du har i området under. Polen Godta tilkobling Broer DNS-port Utgangsnode HTTP-port Utskiftbare transporter SOCKS-port Broer Generelt Porter Mellomtjener er slått på Mellomtjener er slått av Romania Kjører Russland Lagre broer i en fil i ditt lokaloppsett Seychellene Funksjonalitet Generelt Singapore Snowflake Spania _Avbryt Stoppet Sverige Sveits Landet du ønsker å koble til fra Du ar disse valgene for å skaffe bruer :

• Besøk nettstedet til Tor: <a href="https://bridges.torproject.org/options/">https://bridges.torproject.org/options</a>
• Bruk Telegramroboten for Tor-bruker: <a href="https://t.me/GetBridgesBot">Tor bridges Telegram bot</a>
• Send en epost fra en Riseup- eller Gmail-adresse til: <a href="mailto:bridges@torproject.org?body=get bridges">bridges@torproject.org</a> Tractor kunne ikke koble til Traktoren er tilkoblet Tractor kjører ikke! Tractor kjører Tractor er stoppet Kjørbar transport Type transport Ukraina Storbritannia Amerikas forente stater Uten endringer WebTunnel Du har en ny identitet! _Koble til _Koble fra _OK _Lagre Koble til / fra Ny ID Brukervalg Avslutt Snarveier Hovedmeny Vis detaljer Allan Nordhøy, <epost@anotheragency.no>,
Petter Reinholdtsen 