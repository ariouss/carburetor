# Part of Carburetor project
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020-2023.

"""
actions for carburetor
"""

import os
import re
import signal
from subprocess import PIPE, Popen

from gi.repository import Adw, Gio, GLib
from tractor import actions as tactions
from tractor import bridges, checks, db, proxy

from . import config, ui


def add(name: str, callback, app) -> None:
    """
    adds callbacks to app as actions
    """
    action = Gio.SimpleAction.new(name, None)
    action.connect("activate", callback, app)
    app.add_action(action)


def do_startup(app) -> None:
    """
    actions to do when starting the app up
    """
    action_list = [
        ("preferences", on_preferences),
        ("about", on_about),
        ("show-help-overlay", on_show_help_overlay),
        ("quit", on_quit),
        ("connect", on_connect),
        ("new_id", on_new_id),
        ("check_connection", on_check),
        ("toggle_proxy", on_toggle_proxy),
        ("cancel", on_cancel),
        ("save", on_save),
    ]
    for action, callback in action_list:
        add(action, callback, app)


def on_preferences(*argv) -> None:
    """
    show the preferences window
    """
    app = argv[2]
    if not app.prefs:
        prefs_window = ui.get("PreferencesWindow")
        prefs_window.set_transient_for(app.window)
        app.prefs = prefs_window
        action = Gio.SimpleAction.new("save")
        action.connect("activate", on_save)
        action_group = Gio.SimpleActionGroup()
        action_group.add_action(action)
        prefs_window.insert_action_group("app", action_group)
    app.prefs.show()


def on_show_help_overlay(*argv) -> None:
    """
    show the shortcuts window
    """
    app = argv[2]
    if not app.shortcuts:
        shortcuts_window = ui.get("help_overlay_w")
        shortcuts_window.set_transient_for(app.window)
        app.shortcuts = shortcuts_window
    app.shortcuts.show()


def on_about(*argv) -> None:
    """
    show the about window
    """
    app = argv[2]
    if not app.about:
        about_window = Adw.AboutWindow.new_from_appdata(
            "metainfo/io.frama.tractor.carburetor.metainfo.xml"
        )
        about_window.set_developers(["Danial Behzadi <dani.behzi@ubuntu.com>"])
        about_window.set_translator_credits(config._("translator-credits"))
        about_window.set_transient_for(app.window)
        about_window.set_hide_on_close(True)
        app.about = about_window
    app.about.show()


def on_quit(*argv) -> None:
    """
    exit the app
    """
    tactions.kill_tor()
    app = argv[2]
    app.quit()


def on_connect(*argv) -> None:
    """
    clicking on connect button
    """
    app = argv[2]
    button = ui.get("SplitButton")
    button.set_sensitive(False)
    progress_bar = ui.get("ProgressBar")
    ui.set_progress_bar(0)
    progress_bar.show()
    cancel_button = ui.get("CancelButton")
    cancel_button.set_visible(True)
    ui.set_orbi("load")
    page = ui.get("MainPage")
    window = ui.get("MainWindow")
    if checks.running():
        text_stopping = config._("Disconnecting…")
        page.set_title(text_stopping)
        action = "stop"
        window.set_hide_on_close(False)
    else:
        text_starting = config._("Connecting…")
        page.set_title(text_starting)
        action = "start"
        window.set_hide_on_close(True)
    connect(action, app)


def on_new_id(*_, **__) -> None:
    """
    clicking on new id button
    """
    if checks.running():
        tactions.new_id()
        toast = config._("You have a new identity!")
    else:
        toast = config._("Tractor is not running!")
    ui.notify(toast)


def on_check(*_, **__) -> None:
    """
    checks if tractor is connected or not
    """
    ui.set_orbi("load")
    task = Gio.Task.new(
        source_object=None,
        cancellable=None,
        callback=connected_checked,
        callback_data=None,
    )
    task.run_in_thread(check_connected_async)


def on_toggle_proxy(*_, **__) -> None:
    """
    toggle proxy mode on system
    """
    if checks.proxy_set():
        proxy.proxy_unset()
        toast = config._("Proxy has been unset")
    else:
        proxy.proxy_set()
        toast = config._("Proxy has been set")
    ui.notify(toast)


def on_cancel(*_, **__) -> None:
    """
    abort the connection
    """
    dconf = config.dconf
    pid = dconf.get_int("pid")
    os.killpg(os.getpgid(pid), signal.SIGTERM)
    dconf.reset("pid")
    window = ui.get("MainWindow")
    window.set_hide_on_close(False)


def on_save(*_, **__) -> None:
    """
    clicking on save button in bridges
    """
    textview = ui.get("BridgesTextView")
    buff = textview.get_buffer()
    text = buff.get_text(buff.get_start_iter(), buff.get_end_iter(), 0)
    pt_type = db.get_val("bridge-type")
    try:
        relevant = bridges.relevant_lines(text, pt_type)
    except ValueError:
        relevant = True
    if not relevant:
        dialog = ui.get("BridgErrorDialog")
        dialog.show()
    bridges_file = bridges.get_file()
    with open(bridges_file, "w", encoding="utf-8") as file:
        file.write(text)


def connect(action: str, app) -> None:
    """
    connect or disconnect
    """
    task = Popen(
        [config.COMMAND, action, "--verbose"],
        stdout=PIPE,
        start_new_session=True,
    )
    if action == "start":
        config.dconf.set_int("pid", task.pid)
    elif checks.proxy_set():
        proxy.proxy_unset()
    app.io_in = GLib.io_add_watch(task.stdout, GLib.IO_IN, set_progress)
    GLib.io_add_watch(task.stdout, GLib.IO_HUP, thread_finished, app)


def set_progress(stdout, *_) -> bool:
    """
    set progress output on UI
    """
    try:
        line = stdout.readline().decode("utf-8")
        ui.add_to_terminal(line[5:-5])
        if "Bootstrapped" in line:
            valid = re.compile(r".*Bootstrapped .+% \(.*\): ")
            notice = valid.sub("", line)[:-5]
            ui.set_description(notice)
            percentage = line.split(" ")[5]
            ui.set_progress_bar(int(percentage[:-1]))
    except ValueError:
        return False
    return True


def thread_finished(stdout, condition, app) -> bool:
    """
    things to do after process finished
    """
    if condition:
        GLib.source_remove(app.io_in)
        stdout.close()
        ui.set_run_status(app)
        return False
    return True


def check_connected_async(task, *_):
    """
    wrapper to run checks.connected async
    """
    result = checks.connected()
    task.return_boolean(result)


def connected_checked(*argv) -> None:
    """
    Callback for checking connection
    """
    res = argv[1]
    if Gio.Task.is_valid(res):
        result = res.propagate_boolean()
    if result:
        toast = config._("Tractor is connected")
        ui.set_orbi("run")
    else:
        toast = config._("Tractor couldn't connect")
        ui.set_orbi("dead")
    ui.notify(toast)
